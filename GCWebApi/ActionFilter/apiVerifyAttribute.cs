﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using GCWebApi.Extend;

namespace GCWebApi.ActionFilter
{

    //參考: http://blog.xuite.net/f8789/DCLoveEP/29454536-ASP.NET+MVC+-+%E5%AF%A6%E4%BD%9C+ActionFilter+
    public class apiVerifyAttribute: System.Web.Mvc.ActionFilterAttribute
    {
        //在執行 Action 之前執行
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            //var a = filterContext.HttpContext.Request.Headers.GetValues("apiKey");
            //if (filterContext.HttpContext.Request.Headers.GetValues("apiKey") != null)
            //{
            //    if (!filterContext.HttpContext.Request.Headers.GetValues("apiKey").isVerifyApiKeyPass())
            //    {
            //        filterContext.Result = new RedirectResult("/ErrorPage/");
            //    }
            //}
            //else
            //{
            //    filterContext.Result = new RedirectResult("/ErrorPage/");
            //}
        }

        //在執行 Action 之後執行
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        //在執行 Action Result 之前執行
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
           
        }

        //在執行 Action Result 之後執行
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }

    }
}