﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Text;
using System.Collections.Specialized;

namespace GCWebApi.Areas.TestTools.Controllers
{
    public class ToolsController : Controller
    {
        // GET: TestTools/Tools
        public ActionResult Index()
        {
            string DomainUrl = Request.Url.GetLeftPart(UriPartial.Authority);
            string ApiUrl = string.Format("{0}/api/values", DomainUrl);
            GetResult viewModel = new GetResult();
            WebClient wClient = new WebClient();
            wClient.Encoding = Encoding.UTF8;
            wClient.Headers.Add("apiKey:77fc132d-3e70-4df8-9180-d2e1af68479f");
             viewModel.resultstring = wClient.DownloadString(ApiUrl);
            return View(viewModel);
        }

        public ActionResult TryPut()
        {
            string DomainUrl = Request.Url.GetLeftPart(UriPartial.Authority);
            string ApiUrl = string.Format("{0}/api/values?SampleID={1}", DomainUrl,2);
            GetResult viewModel = new GetResult();
            WebClient wClient = new WebClient();
            wClient.Encoding = Encoding.UTF8;
            wClient.Headers.Add("apiKey:77fc132d-3e70-4df8-9180-d2e1af68479f");
            wClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            NameValueCollection MyParameters = new NameValueCollection();
            //MyParameters.Add("SampleID",(2).ToString());
            MyParameters.Add("SampleName", "Jake");
            MyParameters.Add("SampleContent", "HiHi");
            var x = wClient.UploadValues(ApiUrl, "PUT", MyParameters);
            return View();
        }

        public void TryDelete(int SampleID)
        {
            string DomainUrl = Request.Url.GetLeftPart(UriPartial.Authority);
            string ApiUrl = string.Format("{0}/api/values?SampleID={1}", DomainUrl, SampleID);
            GetResult viewModel = new GetResult();
            WebClient wClient = new WebClient();
            wClient.Encoding = Encoding.UTF8;
            wClient.Headers.Add("apiKey:77fc132d-3e70-4df8-9180-d2e1af68479f");
            wClient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            var x = wClient.UploadString(ApiUrl,"DELETE","");
            //return View();
        }
        
    }

    public class GetResult
    {
        public string resultstring { set; get; }
        public GetResult()
        {
            resultstring = string.Empty;
        }
    }
}