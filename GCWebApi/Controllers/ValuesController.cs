﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GCWebApi;
using Newtonsoft.Json;
using GCWebApi.Extend;
using GCWebApi.ActionFilter;

namespace GCWebApi.Controllers
{
    //[Authorize]
    /// <summary>
    /// 測試Sample
    /// </summary>
    public class ValuesController : ApiBaseController
    {
        // GET api/values
        /// <summary>
        /// 用來取資料用沒有參數
        /// </summary>
        /// <returns></returns>
        [apiFilter]
        public List<MySample> Get()
        {
            List<MySample> dbdate = new List<MySample>();
            dbdate = gccorp_db.MySample.Select(x => x).ToList();
            //dbdate = gccorp_db.Database.ExecuteSqlCommand("select * form MySample");
            return dbdate;
        }

        // GET api/values/5
        /// <summary>
        /// 用來取資料用有參數
        /// </summary>
        /// <param name="SampleID">項目編號</param>
        /// <returns></returns>
        public MySample Get(int SampleID)
        {
            MySample dbdate = gccorp_db.MySample.Where(x=>x.SampleID == SampleID).Select(x => x).First();

            return dbdate;
        }



        // POST api/values
        /// <summary>
        /// 用來新增資料
        /// </summary>
        /// <param name="value"></param>
        [apiFilter]
        [HttpPost]
        public HttpResponseMessage Post([FromBody]MySample value)
        {
            try
            {
                value.CreateDate = DateTime.Now.ToUniversalTime();
                value.ModifyDate = DateTime.Now.ToUniversalTime();
                gccorp_db.MySample.Add(value);
                gccorp_db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, value);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }

        // PUT api/values/5
        /// <summary>
        /// 用來修改資料
        /// </summary>
        /// <param name="SampleID"></param>
        /// <param name="value"></param>
         [apiFilter]
         [HttpPut]
        public HttpResponseMessage Put(int SampleID, [FromBody]MySample value)
        {
            try
            {
                var original = gccorp_db.MySample.Where(x => x.SampleID == SampleID).First();
                original.SampleName = value.SampleName;
                original.SampleContent = value.SampleContent;
                original.ModifyDate = DateTime.Now.ToUniversalTime();
                gccorp_db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, original);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,ex.Message);
            }
        }

        // DELETE api/values/5
        /// <summary>
        /// 用來刪除資料
        /// </summary>
        /// <param name="SampleID"></param>
        [apiFilter]
        [HttpDelete]
        public HttpResponseMessage Delete(int SampleID)
        {
            try
            {
                var DelData = gccorp_db.MySample.Where(x => x.SampleID == SampleID).First();
                gccorp_db.MySample.Remove(DelData);
                gccorp_db.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, DelData);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
