﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace GCWebApi.Extend
{
    public static class VerifyApiKey
    {
        public static bool isVerifyApiKeyPass(this IEnumerable<string> apiKey)
        {
            bool VerifyResult = false;
            string sys_apiKey = ConfigurationManager.AppSettings["apiKey"];
            if (apiKey != null)
            {
                if(apiKey.First().ToString() == sys_apiKey)
                {
                    VerifyResult = true;
                }
            }
            return VerifyResult;
        }
    }
}