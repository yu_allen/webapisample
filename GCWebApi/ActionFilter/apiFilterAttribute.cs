﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using GCWebApi.Extend;

namespace GCWebApi.ActionFilter
{
    public class apiFilterAttribute: System.Web.Http.Filters.ActionFilterAttribute
    {
        //在執行 Action 之前執行
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            IEnumerable<string> apiKey;
            bool isGetKey = actionContext.Request.Headers.TryGetValues("apiKey",out apiKey);
            if (isGetKey)
            {
                if (!actionContext.Request.Headers.GetValues("apiKey").isVerifyApiKeyPass())
                {

                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.BadRequest);
                }
            }
            else
            {
                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.BadRequest);
            }
            base.OnActionExecuting(actionContext);
        }

    }
}